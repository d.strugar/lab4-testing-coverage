package com.hw.db.DAO;

import com.hw.db.models.Post;
import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Time;
import java.sql.Timestamp;

@DisplayName("postDAO")
public class postDaoTests {
    static Post getOldPost() {
        return new Post("elon", new Timestamp(1612798884), "some-forum", "some-msg", 0, 0, false);
    }

    // no changes
    @Test
    @DisplayName("basis-setpost#1")
    void testSetPost1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = getOldPost();

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        );
    }

    // everything changes
    @Test
    @DisplayName("basis-setpost#2")
    void testSetPost2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("grimes", new Timestamp(1612798885), "some-forum", "some-other-msg", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    // author changes
    @Test
    @DisplayName("basis-setpost#3")
    void testSetPost3() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("grimes", new Timestamp(1612798884), "some-forum", "some-msg", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    // timestamp changes
    @Test
    @DisplayName("basis-setpost#4")
    void testSetPost4() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("elon", new Timestamp(1612798885), "some-forum", "some-msg", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    // message changes
    @Test
    @DisplayName("basis-setpost#5")
    void testSetPost5() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("elon", new Timestamp(1612798884), "some-forum", "some-other-msg", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    // author + timestamp change
    @Test
    @DisplayName("basis-setpost#6")
    void testSetPost6() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("grimes", new Timestamp(1612798885), "some-forum", "some-msg", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    // message + timestamp change
    @Test
    @DisplayName("basis-setpost#7")
    void testSetPost7() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO p = new PostDAO(mJdbc);

        Post pOld = getOldPost();
        Post pNew = new Post("elon", new Timestamp(1612798885), "some-forum", "some-other-msg", 0, 0, false);

        Mockito.when(mJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(100)
        )).thenReturn(pOld);

        PostDAO.setPost(100, pNew);

        Mockito.verify(mJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }
}