package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.verify;

@DisplayName("forumDAO")
public class forumDaoTests {
    @Test
    @DisplayName("branch-userlist#1")
    void testUserlist1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", null, null, null);
        verify(mJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" + "::citext ORDER BY nickname;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    @DisplayName("branch-userlist#2")
    void testUserlist2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", 10, "01.01.1974", true);
        verify(mJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    @DisplayName("branch-userlist#3")
    void testUserlist3() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        ForumDAO.UserList("slug", 10, "01.01.1974", false);
        verify(mJdbc).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc-threadlist#1")
    void testThreadList1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("slug",null, null, false);
        verify(mJdbc).query(
                Mockito.eq(
                        "SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc-threadlist#2")
    void testThreadList2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("slug",null, "01.01.1974", false);
        verify(mJdbc).query(
                Mockito.eq(
                        "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc-threadlist#3")
    void testThreadList3() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("slug",100, "01.01.1974", true);
        verify(mJdbc).query(
                Mockito.eq(
                        "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc-threadlist#4")
    void testThreadList4() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("slug",null, "01.01.1974", true);
        verify(mJdbc).query(
                Mockito.eq(
                        "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc-threadlist#5")
    void testThreadList5() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("slug",100, null, true);
        verify(mJdbc).query(
                Mockito.eq(
                        "SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("mdmc-threadlist#6")
    void testThreadList6() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mJdbc);
        ForumDAO.ThreadList("slug",100, "01.01.1974", false);
        verify(mJdbc).query(
                Mockito.eq(
                        "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }
}

