package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

@DisplayName("threadDAO")
public class threadDaoTests {
    @Test
    @DisplayName("statement-treesort#1")
    void testTreeSort1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ThreadDAO t = new ThreadDAO(mJdbc);

        ThreadDAO.treeSort(1, null, 50, false);
        Mockito.verify(mJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("statement-treesort#2")
    void testTreeSort2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ThreadDAO t = new ThreadDAO(mJdbc);

        ThreadDAO.treeSort(1, 100, 50, true);
        Mockito.verify(mJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(),
                Mockito.anyInt(),
                Mockito.anyInt()
        );
    }
}