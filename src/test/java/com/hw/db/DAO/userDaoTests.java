package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

@DisplayName("userDAO")
public class userDaoTests {
    // 8 tests required, for every permutation of email/fullname/about

    // 000
    @Test
    @DisplayName("mcdc-change#1")
    void testChange1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", null, null, null));
        verifyNoInteractions(mJdbc);
    }

    // 111
    @Test
    @DisplayName("mcdc-change#2")
    void testChange2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", "elon@spacex.com", "Elon Musk", "Visionary. Doge."));
        verify(mJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    // 001
    @Test
    @DisplayName("mcdc-change#3")
    void testChange3() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", null, null, "Visionary. Doge."));
        verify(mJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    // 010
    @Test
    @DisplayName("mcdc-change#4")
    void testChange4() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", null, "Elon Musk", null));
        verify(mJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    // 011
    @Test
    @DisplayName("mcdc-change#5")
    void testChange5() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", null, "Elon Musk", "Visionary. Doge."));
        verify(mJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    // 100
    @Test
    @DisplayName("mcdc-change#6")
    void testChange6() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", "elon@spacex.com", null, null));
        verify(mJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    // 101
    @Test
    @DisplayName("mcdc-change#7")
    void testChange7() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", "elon@spacex.com", null, "Visionary. Doge."));
        verify(mJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    // 110
    @Test
    @DisplayName("mcdc-change#8")
    void testChange8() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO user = new UserDAO(mJdbc);
        UserDAO.Change(new User("Elon", "elon@spacex.com", "Elon Musk", null));
        verify(mJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }
}
